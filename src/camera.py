import os
import json
import time
import datetime
import numpy as np
import uuid
import math
from io import BytesIO
import cv2
from PyQt5.QtGui import QImage
from qt.image import QNumpyImage
from threading import Lock
import logging
import concurrent.futures


try:
    from picamera import PiCamera
except ImportError:
    from fake_rpi.picamera import PiCamera

def write_counter(fn, count):
    with open(fn, 'w') as f:
        f.write(str(count))


class ImageCounter:
    def __init__(self, storage_dir):
        self.__persitance_storage = \
            os.path.join(storage_dir, '.counter')
        self.__current_counter = None
        self.__executor = concurrent.futures.ThreadPoolExecutor(max_workers=4) 

    def __read(self):
        if not os.path.exists(self.__persitance_storage):
            self.__current_counter = 0
        else:
            with open(self.__persitance_storage, 'r') as f:
                try:
                    self.__current_counter = int(f.read())
                except ValueError:
                    self.__current_counter = 0
        return self.__current_counter

    def __store(self):
        self.__executor.submit(write_counter, 
                               self.__persitance_storage,
                               self.__current_counter)

    @property
    def count(self):
        if self.__current_counter is None:
            self.__current_counter = self.__read()
        return self.__current_counter


    def increase(self):
        self.__current_counter = self.count + 1
        self.__store()

    def __str__(self):
        return str(self.__current_counter)

def write_image(fn, img):
    with open(fn, 'wb') as fp:
        fp.write(img)
    os.sync()

class Camera(object):
    def __init__(self, storage_dir, resolution=(800, 480), framerate=30):
        self.__camera = PiCamera()
        self.__camera.iso = 100
        time.sleep(2)
        self.__camera.shutter_speed = self.__camera.exposure_speed
        self.__camera.exposure_mode = 'off'
        logging.debug(f"camera shutter_speed = {self.__camera.exposure_speed}s")

        self.__resolution = resolution
        self.__image_size = (resolution[0], resolution[1], 3)
        self.__framerate = framerate
        self.__camera_lock = Lock()
        self.__nb_images = ImageCounter(storage_dir)
        self.__storage_dir = storage_dir
        self.__executor = concurrent.futures.ThreadPoolExecutor(max_workers=4) 
        self.__stream = BytesIO()

    def open(self):
        pass

    def image(self, resolution=None):
        with self.__camera_lock:
            if resolution is None:
                self.__camera.resolution = self.__resolution
            else:
                self.__camera.resolution = resolution

            self.__camera.framerate = self.__framerate
            image = np.empty(self.__image_size, dtype=np.uint8)
            self.__camera.capture(image, 'rgb',use_video_port=True)
            qimg = QImage(image,image.shape[0], image.shape[1], QImage.Format_RGB888)
            return qimg


    def __capture(self):
        self.__stream.seek(0)
        next(self.__camera.capture_continuous(self.__stream, 'jpeg', use_video_port=True))
        self.__stream.truncate()
        self.__stream.seek(0)
        yield self.__stream.read()

    def take_photo(self, resolution=None):
        t0 = time.time()
        logging.debug(f"Taking photo with resolution {resolution}")
        with self.__camera_lock:
            if resolution is None:
                self.__camera.resolution = self.__resolution
                resolution = self.__resolution
            else:
                self.__camera.resolution = resolution

            start = time.time()
            image = next(self.__capture())
            end = time.time() - start
            logging.debug("capturing image took %f s" % end)
            ts = datetime.datetime.utcnow().strftime("%Y%m%d-%H%M%S%f")
            img_nr  = self.__nb_images.count + 1
            cap_filename = os.path.join(self.__storage_dir, f'{img_nr}-{ts}.jpg')
            start = time.time()
            self.__executor.submit(write_image, cap_filename, image)
            self.__nb_images.increase()
            end = time.time() - start
            logging.debug("storing image took %f s" % end)
        dur = time.time() - t0
        logging.debug(f"Total capture time: {dur}")

    def nb_images(self):
        return self.__nb_images.count


    def led_on(self):
        pass

    def led_off(self):
        pass
