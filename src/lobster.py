import sys
from io import BytesIO
import json
import numpy as np
from qt.ui.main import Ui_MainWindow
from PyQt5.QtWidgets import QApplication, QWidget, QGraphicsScene, QMainWindow, QSizePolicy
from PyQt5.QtGui import QImage, QPixmap, QPainter, QPen, QFont, QKeySequence
from PyQt5.QtCore import QTimer, Qt, QEvent
from camera import Camera
from gpiozero import Button, LED
import logging

def filesize(kbytes):
    units =['KiB', 'Mib','GiB','TiB','PiB','EiB','ZiB','YiB']
    if bytes is not None:
        thresh = 1024
        c = 0
        while kbytes > thresh:
            kbytes /= thresh
            c += 1
        return (kbytes, units[c])

    return (0, units[0])




class QMainWidget(QMainWindow):
    def __init__(self, ui_cls, parent=None):
        super(QMainWidget, self).__init__(parent)
        self.ui = ui_cls()
        self.ui.setupUi(self)
        self.flash = LED(20)
        self.led = LED(16)
        self.button = Button(17)
        self.camera = Camera("/data/")
        self.single = False
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_img)
        self.update_img()
        self.button.when_released = self.take_photo
        self.flash_on()
#       self.snap = QShortcut(QKeySequence('Ctrl+F'), self)
#      self.snap.activated.connect(self.take_foto)

    def eventFilter(self, obj, event):
        print(event.type())
        if event.type() in [QEvent.TouchBegin,
                            QEvent.MouseButtonPress]:

            self.take_photo()
            return True
        return super().eventFilter(obj, event)


    def __del__(self):
        """
        we make sure the flash is of if the lobster cam stop for some reason
        """
        self.flash_off()

    def update_img(self):
        self.timer.start(1000/15)
        image = self.camera.image()
        p = QPainter()
        p.begin(image)
        p.setPen(QPen(Qt.red))
        p.setFont(QFont("Times", 50, QFont.Bold));
        p.drawText(image.rect(), Qt.AlignCenter, str(self.camera.nb_images()));
        p.end()
#        self.ui.camera.setPixmap(QPixmap(image.scaledToHeight(self.ui.camera.height())))
        self.ui.camera.setPixmap(QPixmap(image))
        self.ui.camera.setScaledContents(True)
#        self.ui.camera.setSizePolicy(
#        self.update_state()

    def flash_on(self):
        self.flash.on()

    def flash_off(self):
        self.flash.off()


    def led_on(self):
        self.led.on()

    def led_off(self):
        self.led.off()


    def take_photo(self):
        self.led_on()
#        self.camera.take_photo(resolution=(1296,730))
        self.camera.take_photo(resolution=(3280,2464))
#        self.camera.take_photo(resolution=(1920, 1080))
        self.led_off()


if __name__ == '__main__':
    logging.basicConfig(filename='lobstercam.log',level=logging.DEBUG)
    try:
        app = QApplication(sys.argv)
        main = QMainWidget(Ui_MainWindow)
        main.installEventFilter(main)
        main.showFullScreen()
        main.show()
        size = str(main.size())
        logging.info("Main window in fullscreen has size %s" % size)
        app.exec_()
    except Exception as e:
        logging.fatal(f'Uncaught exception {e}')
        sys.exit(1)

