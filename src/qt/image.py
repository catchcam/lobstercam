import numpy as np
from PyQt5.QtGui import QImage

class QNumpyImage(QImage): 
    def __init__(self, ndarray): 
        im = ndarray

        if ndarray is None: 
            super(QNumpyImage, self).__init__() 
        else: 
            color_table = False 
            if im.dtype == np.uint16: 
                im = (im/256) 
                im = im.astype(np.uint8) 
            if im.dtype == np.uint8: 
                if len(im.shape) == 2: 
                    fmt = QImage.Format_Indexed8 
                    color_table = True 
                elif len(im.shape) == 3: 
                    if im.shape[2] == 3: 
                        fmt = QImage.Format_RGB888 
                    elif im.shape[2] == 4: 
                        fmt = QImage.Format_ARGB32 
 
                super(QNumpyImage, self).__init__(im.data, im.shape[1], im.shape[0], im.strides[0], fmt) 
                if color_table: 
                    self.setColorTable(self.gray_color_table) 
            else: 
                raise Exception("Image format not implemented") 
