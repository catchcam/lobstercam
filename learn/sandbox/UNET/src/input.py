import os
import shutil
import random
import numpy as np
from tensorflow import keras
from tensorflow.keras.preprocessing.image import load_img


def split(fraction: float,
              img_dir: str,
              annotation_dir: str,
              train_img_dir: str,
              train_ann_dir: str,
              val_img_dir: str,
              val_ann_dir: str):

    input_img_paths = sorted([
        os.path.join(img_dir, fname) for fname in os.listdir(img_dir)
            if fname.endswith(".jpg")
        ])

    target_img_paths = sorted([
        os.path.join(annotation_dir, fname) for fname in os.listdir(annotation_dir)
            if fname.endswith(".png") and not fname.startswith(".")
        ])

    val_samples = int(len(input_img_paths) * fraction)
    random.Random(1337).shuffle(input_img_paths)
    random.Random(1337).shuffle(target_img_paths)

    train_input_img_paths = input_img_paths[:-val_samples]
    train_annotation_paths = target_img_paths[:-val_samples]
    for p in train_input_img_paths:
        shutil.move(p, train_img_dir)

    for p in train_annotation_paths:
        shutil.move(p, train_ann_dir)

    val_input_img_paths = input_img_paths[-val_samples:]
    val_annotation_paths = target_img_paths[-val_samples:]

    for p in val_input_img_paths:
        shutil.move(p, val_img_dir)

    for p in val_annotation_paths:
        shutil.move(p, val_ann_dir)

class MaskedImages(keras.utils.Sequence):
    def __init__(self, image_dir: str, img_ext: str, annotation_dir: str,
                 batch_size: int, img_size: tuple[int, int]):

        self.__input_img_paths = sorted([
            os.path.join(image_dir, fname)
            for fname in os.listdir(image_dir)
                if fname.endswith(img_ext)
            ])

        self.__annotation_paths = sorted([
            os.path.join(annotation_dir, fname)
            for fname in os.listdir(annotation_dir)
                if fname.endswith(".png") and not fname.startswith(".")
            ])
        self.__batch_size = batch_size
        self.__image_size = img_size


    def __getitem__(self, idx):
        """
        Returns batch idx as a tuple (input, target)
        """
        i = idx * self.__batch_size
        x = np.zeros((self.__batch_size,) \
                      + self.__image_size + (3,), dtype='float32')
        for j, path in \
                enumerate(self.__input_img_paths[i : i + self.__batch_size]):
            img = load_img(path, target_size=self.__image_size)
            x[j] = img

        y = np.zeros((self.__batch_size,) \
                      + self.__image_size + (1,), dtype='uint8')
        for j, path in \
                enumerate(self.__annotation_paths[i: i + self.__batch_size]):
            mask = load_img(path, target_size=self.__image_size, color_mode='grayscale')
            y[j] = np.expand_dims(mask, 2)
            y[j] -= 1

        return x,y


    def img_path(self, i):
        return self.__input_img_paths[i]

    def ann_path(self, i):
        return self.__annotation_paths[i]

    def __len__(self):
        """
        Returns number of batches
        """
        return len(self.__input_img_paths) // self.__batch_size

    def __str__(self):
        return f"Number of batches: {len(self)}\n"



