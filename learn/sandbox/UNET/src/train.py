from tensorflow import keras
from model import mk_model
import input as inp


config = {"img_size": (160,160),
          "nb_classes": 3,
          "batch_size": 64,
          "train_image_dir": "../data/train_img/",
          "train_annotation_dir": "../data/train_ann/",
          "val_image_dir": "../data/val_img/",
          "val_annotation_dir": "../data/val_ann/",
          "epochs": 500
          }

if __name__ == "__main__":
    train_gen = inp.MaskedImages(config["train_image_dir"],
                           ".jpg",
                           config["train_annotation_dir"],
                           config["batch_size"],
                           config["img_size"])
    val_gen = inp.MaskedImages(config["val_image_dir"],
                           ".jpg",
                           config["val_annotation_dir"],
                           config["batch_size"],
                           config["img_size"])
    keras.backend.clear_session()
    model = mk_model(config["img_size"], config["nb_classes"])
#    model.compile(optimizer="rmsprop", loss="sparse_categorical_crossentropy",
    model.compile(optimizer=keras.optimizers.Adam(), loss="sparse_categorical_crossentropy",
                  metrics=['accuracy', 'mse'])
    callbacks = [
        keras.callbacks.ModelCheckpoint("oxford_segmentation.h5",
                                        save_best_only=True),
        keras.callbacks.TensorBoard(log_dir="./logs",  update_freq=1,
                                    write_graph=True),
        keras.callbacks.CSVLogger('history.csv', separator=',', append=False)
        ]

    history = model.fit(train_gen,
              epochs=config["epochs"],
              validation_data=val_gen,
              callbacks=callbacks)
    model.save('lastrun/')

    print(history.history.keys())
    print(history.history['accuracy'])



