from tensorflow import keras
from tensorflow.keras import layers


def __mk_conv_layer(layer_cls, filters, padding, connect_with):
    l = layers.Activation('relu')(connect_with)
    l = layer_cls(filters, 3, padding=padding)(l)
    l = layers.BatchNormalization()(l)
    return l


def mk_model(img_size: tuple[int, int], num_classes: int):
    inputs = layers.Input(shape=img_size + (3,))
    x = layers.Conv2D(32, 3, strides=2, padding='same')(inputs)
    x = layers.BatchNormalization()(x)
    x = layers.Activation("relu")(x)

    previous_block_activation = x

    for filters in [64, 128, 256, 512]:
        x = __mk_conv_layer(layer_cls=layers.SeparableConv2D,
                            filters=filters, padding="same",
                            connect_with=x)

        x = __mk_conv_layer(layer_cls=layers.SeparableConv2D,
                            filters=filters, padding="same",
                            connect_with=x)

        x = layers.MaxPooling2D(3, strides=2, padding="same")(x)
        residual = layers.Conv2D(filters, 1, strides=2, padding="same")(
            previous_block_activation)
        x = layers.add([x, residual])
        previous_block_activation = x

    for filters in [512, 256, 128, 64, 32]:
        x = __mk_conv_layer(layers.Conv2DTranspose,
                            filters=filters, padding="same",
                            connect_with=x)

        x = __mk_conv_layer(layers.Conv2DTranspose,
                            filters=filters, padding="same",
                            connect_with=x)

        x = layers.UpSampling2D(2)(x)

        residual = layers.UpSampling2D(2)(previous_block_activation)
        residual=layers.Conv2D(filters, 1, padding = "same")(residual)
        x=layers.add([x, residual])
        previous_block_activation=x

    outputs=layers.Conv2D(num_classes, 3,
                            activation = "softmax",
                            padding = "same")(x)
    model=keras.Model(inputs, outputs)
    return model
