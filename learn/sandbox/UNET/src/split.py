import os
import random
from input import split
try:
    os.mkdir("../data/train_img/")
except FileExistsError:
    pass

try:
    os.mkdir("../data/train_ann/")
except FileExistsError:
    pass
try:
    os.mkdir("../data/val_img/")
except FileExistsError:
    pass
try:
    os.mkdir("../data/val_ann/")
except FileExistsError:
    pass

split(0.25,
      "../data/images",
      "../data/annotations/trimaps/",
      "../data/train_img/",
      "../data/train_ann/",
      "../data/val_img/",
      "../data/val_ann")
