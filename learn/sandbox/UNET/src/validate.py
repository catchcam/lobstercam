import os
import PIL
import numpy as np
from tensorflow import keras
import input as inp


config = {"img_size": (160,160),
          "nb_classes": 3,
          "batch_size": 16,
          "train_image_dir": "../data/train_img/",
          "train_annotation_dir": "../data/train_ann/",
          "val_image_dir": "../data/val_img/",
          "val_annotation_dir": "../data/val_ann/",
          "epochs": 20
          }


def save_mask(prediction, fname):
    mask = np.argmax(prediction, axis=-1)
    mask = np.expand_dims(mask, axis=-1)
    img = PIL.ImageOps.autocontrast(keras.preprocessing.image.array_to_img(mask))
    img.save(fname)



if __name__ == "__main__":
    val_gen = inp.MaskedImages(config["val_image_dir"],
                           ".jpg",
                           config["val_annotation_dir"],
                           config["batch_size"],
                           config["img_size"])
    keras.backend.clear_session()
    model = keras.models.load_model('lastrun/')

    predictions = model.predict(val_gen)

    for i in range(len(predictions)):
        mask_fn = os.path.basename(val_gen.ann_path(i))
        save_mask(predictions[i], f"predictions/{mask_fn}")
