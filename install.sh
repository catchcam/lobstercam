#! /bin/sh
fstab="LABEL=DATA    /data    vfat    auto,rw,user,sync,exec,dev,suid,uid=500,gid=500,umask=000  0 0"
lobster="@/usr/bin/python3 lobstercam/src/lobster.py"

add(){
	grep -qF "$1" "$2" || echo $1 >> $2
}

sudoadd(){
	grep -qF "$1" "$2" || sudo su -c "echo $1 >> $2"
}

sudo /usr/sbin/locale-gen
sudo apt-get update -y 
sudo apt-get upgrade -y

sudo raspi-config nonint do_expand_rootfs
sudo raspi-config nonint do_camera 1
sudo raspi-config nonint do_hostname "lobstercam"
sudo raspi-config nonint do_boot_behaviour B4
sudo raspi-config nonint do_serial 1
sudo raspi-config nonint do_ssh 0


sudo apt-get install -y pyqt5-dev-tools
sudo apt-get install -y git
sudo apt-get install -y libatlas3-base libatlas* libjasper1
sudo apt-get install -y python3-pyqt5*
sudo apt-get install -y python3-picamera
sudo apt-get install -y python3-gpiozero
sudo apt-get install unclutter
cd ~pi

if [ -d "lobstercam" ]; then
    rm -rf lobstercam
fi

git clone  https://git.wur.nl/catchcam/lobstercam

cd lobstercam/src/qt
make
cd ~pi

#ENABLE STROMPI
sudoadd "dtoverlay=pi3-miniuart-bt" /boot/config.txt 
sudoadd "enable_uart=1" /boot/config.txt  
sudoadd "core_freq=250" /boot/config.txt 
enable_uart=1
core_freq=250
sudo sed /boot/config.txt -i -e "s/^startx/#startx/"
sudo sed /boot/config.txt -i -e "s/^fixup_file/#fixup_file/"
sudoadd "start_x=1" /boot/config.txt 
grep -qF "gpu_mem" /boot/config.txt || sudo su -c "echo gpu_mem=128 >> /boot/config.txt"
echo -e "Lobster\nLobster" | sudo passwd pi
grep -qF "LABEL=DATA" /etc/fstab || sudo su -c "echo $fstab >> /etc/fstab"
grep -qF "consoleblank=0" /boot/cmdline.txt || sudo su -c "sed -i 's/$/ consoleblank=0/'  /boot/cmdline.txt"
sudo sed -i 's/^autologin-user=.*/autologin-user=pi/' /etc/lightdm/lightdm.conf
sudo sed -i s/#autologin-user-timeout/autologin-user-timeout/ /etc/lightdm/lightdm.conf
sudo dpkg-reconfigure lightdm
cp -r /etc/xdg/lxsession ~/.config
add "@unclutter -idle 0" ~/.config/lxsession/LXDE-pi/autostart
add "$lobster" ~/.config/lxsession/LXDE-pi/autostart  
wget https://strompi.joy-it.net/wp-content/uploads/2020/01/StromPi3_Scriptfolder_20200108.zip
sudo reboot
