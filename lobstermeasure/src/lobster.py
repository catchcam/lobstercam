"""
    This file is part of CatchTag.

    CatchTag is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CatchTag is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import csv
import enum
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QGraphicsScene, QGraphicsLineItem, QFileDialog, QGraphicsView, QGraphicsRectItem, QTableWidgetItem, QHeaderView, QGraphicsItem, QGraphicsTextItem
from PyQt5.QtCore import Qt, QRectF, QRect, QPointF, QLineF
from PyQt5.QtGui import QBrush, QPainterPath, QColor, QRadialGradient, QPixmap, QImage, QPainter, QPen, QFont
from qt5.QCatchTag import Ui_CatchTag
import numpy as np
import seaborn as sns
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.pyplot import cla, clf
from models import  FileFault, LengthModel


def plot_histogram(data):
    x = []
    cla()
    clf()
    d = list(data)
    if len(d) > 1:
        bins = (int(max(d)+ 0.5) - int(min(d))) * 2
        if bins == 0:
            bins = 1 
    else:
        bins = 1
    g = sns.distplot(np.array(d), bins=bins, kde=False, color='b')
    return g.get_figure()



class QMainWidget(QWidget):
    def __init__(self, ui_cls, model=None, parent=None):
        super().__init__(parent)
        self.ui = ui_cls()
        self.ui.setupUi(self)
        self.__model = model
        sns.set(rc={'axes.facecolor':"#4f4f4f", 'figure.facecolor':"#4f4f4f"})
        self.scene = LobsterScene(model)
        self.ui.catchcamView.setScene(self.scene)
        self.ui.catchcamView.setDragMode(QGraphicsView.NoDrag)
        self.chart_view = None
        #self.chart_view = QChartView()
        #self.chart_view.setBackgroundBrush(QBrush(QColor(79,79,79)))
        #self.ui.chart_layout.addWidget(self.chart_view)
        self.directory = None
        self.__current_file = None
        self.file_fault = None
        self.__has_image = False
        self.img_rect = None
        self.roi = None
        self.roi_item = None
        self.view_rect = None
        self.zoom_mode = False
        self.report_filename = None 
        self.pos_filename = None
        self.__connect()
        self.__enable_toolbox(False)

    
    def __connect(self):
        self.ui.open.clicked.connect(self.browse)
        self.ui.next_img.clicked.connect(self.next)
        self.ui.prev_img.clicked.connect(self.prev)
        self.ui.zoomin.clicked.connect(self.zoomin)
        self.ui.zoomout.clicked.connect(self.zoomout)
        self.ui.fit.clicked.connect(self.zoomfit)
        #self.ui.select.toggled.connect(self.toggle_select)
        self.ui.catchcamView.rubberBandChanged.connect(self.select_area)
        self.scene.changed.connect(self.save)
        self.scene.changed.connect(self.create_chart)
    
    def __enable_toolbox(self, enabled):
        self.ui.next_img.setEnabled(enabled)
        self.ui.prev_img.setEnabled(enabled)
        self.ui.zoomin.setEnabled(enabled)
        self.ui.zoomout.setEnabled(enabled)
        #self.ui.select.setEnabled(enabled)
        #self.ui.deletepoints.setEnabled(enabled)


    def create_chart(self, region=None):
        data = self.__model.length_freq()
        self.fig = plot_histogram(data)
        if self.chart_view:
            self.ui.chart_layout.removeWidget(self.chart_view)
        self.chart_view = FigureCanvas(self.fig)
        self.ui.chart_layout.addWidget(self.chart_view)
        pass

    def zoomin(self):
        if not self.__has_image:
            return
        if self.view_rect:
            self.view_rect.setHeight(self.view_rect.height() / 1.2)
            self.view_rect.setWidth(self.view_rect.width() / 1.2)
            self.ui.catchcamView.fitInView(self.view_rect, mode=Qt.KeepAspectRatio)
    
    def zoomout(self):
        if not self.__has_image:
            return
        if self.view_rect:
            self.view_rect.setHeight(self.view_rect.height() * 1.2)
            self.view_rect.setWidth(self.view_rect.width() * 1.2)
            self.ui.catchcamView.fitInView(self.view_rect, mode=Qt.KeepAspectRatio)
    
    def zoomfit(self):
        if not self.__has_image:
            return
        self.ui.catchcamView.fitInView(self.img_rect, mode=Qt.KeepAspectRatio)

    def select_area(self, rect, point):
        if not self.__has_image:
            return
        if rect.isNull():
            if self.view_rect:
                self.roi = self.view_rect
                self.scene.set_mode(SceneMode.ADD)
                self.ui.select.setChecked(False)
        else:
            p =  self.ui.catchcamView.mapToScene(rect)
            self.view_rect = p.boundingRect()

    def next(self):
        if self.file_fault:
            next_img = self.file_fault.next()
            self.update_img(next_img)

    def prev(self):
        if self.file_fault:
            prev_img = self.file_fault.prev()
            self.update_img(prev_img)

    def update_img(self, img):
        if img:
            line = None
            self.current_file = img
            if self.__model:
                line = self.__model.set_target(img)
            roi = self.roi
            self.scene.clear()
            if roi:
                self.__show_roi(roi)

            if line:
                self.__set_line(line)
            
            self.ui.fileindex.setText(self.file_fault.index()) 
        else:
            self.ui.fileindex.setText("")

    def __set_line(self, line):
        self.scene.start_line(line.x1, line.y1)
        self.scene.end_line(line.x2, line.y2)
        
    @property
    def roi(self):
        if self.__model:
            r = self.__model.roi
        else:
            return None

        if r:
            return QRectF(r[0], r[1], r[2], r[3])
        else: 
            return None

    @roi.setter
    def roi(self, rect):
        if rect:
            r = (rect.x(),
                 rect.y(),
                 rect.width(),
                 rect.height())
            if self.__model:
                self.__model.roi = r
            self.__show_roi(rect)

    def __show_roi(self, rect):
        self.roi_item = QGraphicsRectItem(rect)
        self.roi_item.setPen(QColor(255,0,0))
        self.scene.addItem(self.roi_item)
        self.ui.catchcamView.fitInView(rect, mode=Qt.KeepAspectRatio)

    @property
    def current_file(self):
       return self.__current_file 

    @current_file.setter
    def current_file(self, filename):
        self.__current_file = filename
        image = QPixmap(self.current_file)
        self.scene.set_background(image)
        self.img_rect =  QRectF(image.rect())
        if not self.roi:
            self.roi = self.img_rect
            self.view_rect = self.img_rect
        else:
            self.view_rect = self.roi
        self.scene.setSceneRect(self.img_rect)
        self.ui.catchcamView.fitInView(self.view_rect, mode=Qt.KeepAspectRatio)
        self.__has_image = True

    def browse(self):
        directory = QFileDialog.getExistingDirectory(self, "LobsterCam Image directory")
        if directory:
            self.directory = directory
            self.report_filename = os.path.join(directory, 'length.csv')
            self.pos_filename = os.path.join(directory, '.meta.dat')
            self.file_fault = FileFault(self.directory, ext="jpg;jpeg;png".split(';'))
            self.ui.fileindex.setText(self.file_fault.index()) 
            self.ui.cwd.setText(self.directory)
            if os.path.exists(self.pos_filename):
                self.load()
            self.__enable_toolbox(True)
            self.update_img(self.file_fault.current())

    def save(self):
        if self.__model and self.pos_filename:
            self.__model.save_lines(self.pos_filename)
            self.__model.save_report(self.report_filename)
        #    print(self.__model.length_freq())

    def load(self):
        if self.__model and self.pos_filename:
            self.scene.clear()
            try:
                self.__model.load_lines(self.pos_filename)
            except ValueError:
                pass # we should do something here but for the demo it is ok

class SceneMode(enum.Enum):
    START,END=range(2)

class LobsterScene(QGraphicsScene):
    def __init__(self, model=None, parent=None):
        super().__init__(parent)
        self.background = None
        self.model = model
        self.bg_rect = None
        self.view_rect = None
        self.delete_radius = 16
        self.__mode = SceneMode.START
        self.__current_line = None

    def drawBackground(self, painter, rect):
        if self.background:
            painter.drawPixmap(QRect(0,0, self.width(), self.height()), 
                self.background)

    def set_background(self, qpixmap):
        """
            set new background image
        """
        self.background = qpixmap
        self.bg_rect = QRectF(qpixmap.rect())
        self.view_rect = self.bg_rect 
        self.update()

    def start_line(self, x,y):
            self.clear()
            item = LineItem(x,y)
            item.setEnd(x,y)
            self.__current_text = None
            self.__current_line = item
            self.addItem(item)


    def end_line(self, x, y):
        if self.__current_line:
            self.__current_line.setEnd(x, y)
            self.__current_text = QGraphicsTextItem(parent=self.__current_line)
            self.__current_text.setHtml(str(self.__current_line.length))
            self.__current_text.setTextWidth(400)
            self.__current_text.setFont(QFont("Helvetica [Cronyx]", 20, QFont.Bold))
            self.__current_text.setDefaultTextColor(QColor(200,0,0))
            self.__current_text.setPos(x,y)
            self.model.set(self.__current_line.start_point,
                           self.__current_line.end_point,
                           self.__current_line.length)
            self.update_length()

    def update_length(self):
        if self.__current_line.length > 0:
            if self.__current_text:
                length = "%.1f cm" % self.model.calc_length(self.__current_line.length)
                self.__current_text.setHtml(length)

    def mousePressEvent(self, event):
        if not self.background:
            return
        pos = event.scenePos()
        if self.__mode == SceneMode.START and event.button() == Qt.LeftButton:
            self.start_line(pos.x(), pos.y())
            self.__mode = SceneMode.END

    def mouseReleaseEvent(self, event):
        if not self.background:
            return
        pos = event.scenePos()
        if self.__mode == SceneMode.END and event.button() == Qt.LeftButton:
            self.end_line(pos.x(), pos.y())
            self.__mode = SceneMode.START

    def mouseMoveEvent(self, event):
        if not self.background:
            return
        if self.__mode == SceneMode.END and event.buttons() == Qt.LeftButton:
            pos = event.scenePos()
            self.__current_line.setEnd(pos.x(), pos.y())
            self.update_length()


class LineItem(QGraphicsLineItem):
    def __init__(self, x, y, parent=None):
        super().__init__(parent)
        pen = QPen(QColor(200,0,0))
        pen.setWidth(5)
        pen.setCapStyle(Qt.RoundCap)
        self.setPen(pen)
        self.start_point = QPointF(x, y)
        self.end_point = QPointF(x, y)
        self.setLine(QLineF(self.start_point, self.end_point))
    
    def setEnd(self, x,y):
        self.end_point = QPointF(x,y)
        self.setLine(QLineF(self.start_point, self.end_point))
    
    @property
    def length(self):
        return abs(self.line().length())

if __name__ == '__main__':
    app = QApplication(sys.argv)
    central = QMainWidget(Ui_CatchTag, model=LengthModel())
    main = QMainWindow()
    main.setStyleSheet('background-color: rgb(79, 79, 79);font: 87 12pt "Source Code Pro for Powerline";color: rgb(128, 255, 255);')
    main.setCentralWidget(central)
    main.setWindowTitle("LobsterCam")

    main.show()
    app.exec_()
