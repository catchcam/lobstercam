"""
    This file is part of CatchTag.

    CatchTag is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CatchTag is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import glob
import csv
import uuid

from collections import Counter
from collections import defaultdict

def between(value, interval):
    return value > interval[0] and value < interval[1]

def inside(p, rect):
    x = p.x()
    y = p.y()
    if between(x, (rect[0], rect[0] + rect[2])) \
        and between(y, (rect[1], rect[1] + rect[3])):
            return True
    return False


class Point(object):
    def __init__(self, p):
        self.__x = p[0]
        self.__y = p[1]

    def x(self):
        return self.__x

    def y(self):
        return self.__y



class FileFault(object):
    def __init__(self, directory, ext=None):
        self.directory = directory
        self.files = []
        if isinstance(ext, list):
            for e in ext:
                wc = "*.%s" % e
                self.files.extend(glob.glob(os.path.join(self.directory, wc)))
        else:
            self.files = glob.glob(self.directory)

        self.files = [os.path.basename(p) for p in self.files]
        self.nb_files = len(self.files)

        self.__current = 0

    def current(self):
        try:
            return os.path.join(self.directory, self.files[self.__current])
        except IndexError:
            return None

    def next(self):
        l = len(self.files)
        if l > 0:
            self.__current += 1
            if not self.__current < l:
                self.__current = l - 1
            return os.path.join(self.directory, self.files[self.__current])
        return None

    def prev(self):
        l = len(self.files)
        if l > 0:
            self.__current -= 1
            if not self.__current >= 0:
                self.__current = 0
            return os.path.join(self.directory, self.files[self.__current])
        return None

    def index(self):
        return "%d/%d" %(self.__current + 1, self.nb_files)

    def goto(self, filename):
        self.__current = self.files.index(os.path.basename(filename))


class LengthModel(object):
    def __init__(self):
        super().__init__()
        self.lines = {}
        self.rois = {}
        self.__target = None
        self.ppcm = 49 #pixels per centimeter, we have to figure this out for the demo

    def set_target(self, target):
        if target is not None:
            self.__target = target
            if target not in self.lines:
                self.lines[target] = LineModel((0,0), (0,0), 0, 0)    
            return self.lines[target]
        return None

    def length_freq(self):
        return [line.length_cm for line in self.lines.values() if line.length_cm > 0.0]

    def calc_length(self, length_px):
        return length_px / self.ppcm

    @property
    def roi(self):
        if self.__target in self.rois:
           return self.rois[self.__target]
        else:
            return None

    @roi.setter
    def roi(self, rect):
        if self.__target:
            self.rois[self.__target] = rect

    def set(self, start, end, length_px):
        if self.__target:
            if self.__target in self.rois:
                roi = self.rois[self.__target]
                if not inside(start, self.rois[self.__target]):
                    return None
            else:
                roi = None
            length_cm = self.calc_length(length_px)
            model = LineModel((start.x(), start.y()), (end.x(), end.y()), length_cm, length_px)
            
            self.lines[self.__target] = model
            return model
        return None

    def delete(self):
        for tag in self.lines[self.__target]: #short lists so lets use bf search
                self.lines[self.__target] = None
                return True
        return False
        

    def save_report(self, filename):
        with open(filename, 'w') as f:
            f.writelines(["%s,%s\n" % (i[0], str(i[1])) for i in self.lines.items()])

    def save_lines(self, filename):
        with open(filename, 'w') as f:
            if self.lines:
                for t in self.lines:
                    if t in self.rois:
                        roi = str(self.rois[t])
                    else: 
                        roi = ""

                    f.write("<%s|%s>\n" % (t, roi))
                    f.write("%s\n" % self.lines[t].to_str())


    def load_lines(self, filename):
        with open(filename, 'r') as f:
            self.clear()
            for l in f.readlines():
                l = l.strip()
                if l:
                    if l.startswith("<"):
                        l = l.strip("<")
                        l = l.strip(">")
                        tokens = l.split('|')
                        if len(tokens) > 1:
                            target = tokens[0]
                            roi = tuple([float(v) for v in tokens[1].strip('()').split(',')])
                            self.__target = target
                            self.roi = roi
                    else:
                        self.lines[self.__target] = LineModel.from_str(l)
                        print(self.lines[self.__target])

    def clear(self):
        self.lines= {}
        self.rois = {}
        self.__target = None

class LineModel():
    def __init__(self, start, end, length_cm, length_px):
        self.x1 = float(start[0])
        self.x2 = float(end[0])
        self.y1 = float(start[1])
        self.y2 = float(end[1])
        self.length_cm = float(length_cm)
        self.length_px = int(length_px)
    
    def __str__(self):
        return f'{self.length_cm} cm'

    def to_str(self):
        return f'{self.x1},{self.y1},{self.x2},{self.y2}, {self.length_cm}, {self.length_px}'

    @staticmethod
    def from_str(s):
        x1, y1, x2, y2, length_cm, length_px = s.split(',')
        return LineModel((x1, y1), (x2,y2), length_cm, length_px)



    

